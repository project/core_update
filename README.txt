Core Update Module
==================
Provides the capability to update Drupal 7 core from your web browser.

This module may be installed and enabled like a standard Drupal 7 module. Once
enabled, a button is added to the Update tab of the Available Updates report
(/admin/reports/updates/update) whenever there is a newer release of Drupal 7
than the version your site is running.

After a successful update, you'll be redirected to Drupal's database schema
updates page. This page will take care of any corresponding database updates.

The most important things to know about this module are:
  * Rather than downloading an entire release .zip and re-installing all of
    Drupal, it downloads a release artifact containing only the files that
    changed between the Drupal releases. While this allows the update to
    progress as quickly and non-disruptively to your site as possible, it
    currently means the code changes are fetched from a site that's not
    affiliated with the Drupal Association.
  * It does not yet have support for updating your site over FTP. Your Drupal
    core files must be writable by the webserver. If a file that needs to be
    changed is not writable, the module will roll other files back to their
    pre-update state and your site will continue to operate under the old
    version until the problem can be corrected.
    -> The underlying Curator project has FTP support; if you need this feature
    please contact the maintainer to help gauge demand.

While Drupal Core files are being written, Drupal is not bootstrapped by this
module. This offers some protection against updates failing partway through
because Drupal is itself in a partially-updated / inconsistent / broken state.
A separate entry point ("index.php") script distributed with this module
handles the requests from your browser necessary to perform the update during
this time. The file drupal_curator.php must be allowed to run as a php script
for this module to operate.

This module is powered by the Curator project
  Coppyright (c) 2015-2019 Mike Baynton
  License: MIT
  https://github.com/curator-wik/curator
