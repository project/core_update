<?php

/**
 * @file
 * Alternate entrypoint that fields the requests for an attended core update.
 */

$phar = __DIR__ . DIRECTORY_SEPARATOR . 'drupal_curator.phar';

// You have no business with any code in the .phar unless you are an
// authenticated user with D7's administer software updates permission who
// clicked the update core button.
// As a general vulnerability mitigation, make sure that's you first.
session_name('CURATOR_' . substr(md5($phar), 0, 8));

$session_use_cookies = ini_get('session.use_cookies');

if (isset($_COOKIE[session_name()])) {
  ini_set('session.use_cookies', '0');
  session_id($_COOKIE[session_name()]);
  session_start();
}

$is_authenticated = !empty($_SESSION['_sf2_attributes']['IsAuthenticated']);

if (!$is_authenticated) {
  if (session_status() == PHP_SESSION_ACTIVE) {
    session_destroy();
  }
  header('HTTP/1.0 403 Forbidden');
  exit;
}

if (function_exists('session_abort')) {
  session_abort();
}
else {
  session_write_close();
}
ini_set('session.use_cookies', $session_use_cookies);

/**
 * @var \Curator\AppManager $appManager
 * The interface between integration scripts like this one and Curator.
 */
$appManager = require $phar;
unset($phar);
$appManager->run();
