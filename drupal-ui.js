(function($) {
  /**
   * Attaches the core_update behavior to progress bars.
   */
  Drupal.behaviors.core_update = {
    attach: function(context, settings) {
      $("#progress", context).once("batch", function() {
        var hasErrors = false;
        var holder = $(this);

        var progress = new Drupal.progressBar("updateprogress");
        // patch in a function for showing errors without hiding the progress bar.
        progress.appendError = function(string) {
          var error = $('<div class="messages error"></div>').html(string);
          $("#errors").append(error);

          if (this.errorCallback) {
            this.errorCallback(this);
          }
        };
        progress.setProgress(-1, "Initializing...");
        holder.append(progress.element);

        var cc = window.CuratorBatchClient;
        var currPct = -1;
        var currMsg = "";
        var bindProgressBar = function() {
          progress.setProgress(currPct, currMsg);
        };
        var messageCallback = function(granularity, message) {
          if (granularity >= cc.GRANULARITY_TASKGROUP) {
            currMsg = message;
            bindProgressBar();
          }
        };
        var progressCallback = function(granularity, pct) {
          if (granularity >= cc.GRANULARITY_TASKGROUP) {
            currPct = Math.round(pct);
            bindProgressBar();
          }
        };
        var errorCallback = function(chatterArray) {
          hasErrors = true;
          if (chatterArray.length > 0) {
            progress.appendError(chatterArray.join("<br />"));
          } else {
            progress.appendError("Oops! Something went wrong...");
          }
        };
        var completeCallback = function() {
          if (!hasErrors) {
            window.location = settings.curator.post_update_url;
          }
        };
        cc.go(
          settings.curator.api_endpoint,
          messageCallback,
          progressCallback,
          completeCallback,
          errorCallback
        );
      });
    }
  };
})(jQuery);
